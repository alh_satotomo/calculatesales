package jp.alhinc.sato_tomo.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CalculateSales {

	public static void main (String[] args) {
		HashMap<String,String> branchMap = new HashMap<String,String>();
		HashMap<String,Long> salesMap = new HashMap<String,Long>();

		if(!fileRead(args[0],"branch.lst",branchMap,salesMap)) {
			return;
		}

		BufferedReader br1 = null;
		FileReader fr1 = null;

		File dir = new File(args[0]);
		File[] list = dir.listFiles();

		ArrayList <File> list1 = new ArrayList<File>();

		for(int i = 0; i < list.length ; i++) {
			String str = list[i].getName() ;

			if(str.matches("[0-9]{8}.*rcd") && list[i].isFile()) {
				list1.add(list[i]);
			}
		}

		for(int i = 0; i < list1.size() - 1 ; i++) {
			String str1 = list1.get(i).getName();
			String str2 = list1.get(i+1).getName();
			String new_str = str1.substring(0,8);
			String new_str1 = str2.substring(0,8);
			int salesFileNumber = Integer.parseInt(new_str);
			int salesFileNumber1 = Integer.parseInt(new_str1);

			if(salesFileNumber1 - salesFileNumber != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		for(int i = 0; i< list1.size() ; i++) {
			try {
				ArrayList<String> list2 = new ArrayList<String>();
				fr1 = new FileReader(list1.get(i));
				br1 = new BufferedReader(fr1);
				String line1;

				while((line1 = br1.readLine()) != null) {
					list2.add(line1);
				}

				if(list2.size() != 2) {
					System.out.println(list1.get(i).getName() + "のフォーマットが不正です");
					return;
				}

				if(!list2.get(1).matches("[0-9]{0,}")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				if(!branchMap.containsKey(list2.get(0))) {
					System.out.println(list1.get(i).getName() + "の支店コードが不正です");
					return;
				}

				long sales = Long.parseLong(list2.get(1))+salesMap.get(list2.get(0));
				if(sales >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				salesMap.put(list2.get(0),sales);

			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;

			}finally {
				if(br1 != null) {

					try {
						br1.close();
						fr1.close();

					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;

					}
				}
			}

			if(!fileWrite(args[0],"branch.out", branchMap, salesMap)) {
				return;
			}
		}
	}

	public static boolean fileRead(String directry,String LstfileName ,HashMap<String,String> branchMap,HashMap<String,Long> salesMap) {
		BufferedReader br = null ;
		if(directry.length() != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}

		try {
			File branchfile = new File(directry,LstfileName);

			if(!branchfile.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(branchfile);
			br = new BufferedReader(fr);
			String line;

			while((line = br.readLine()) != null) {
				String[] coad = line.split(",");

				if(coad.length != 2 || !coad[0].matches("[0-9]{3}")){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}

				branchMap.put(coad[0], coad[1]);
				salesMap.put(coad[0], 0L);
			}

		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;

		}finally {
			if(br != null) {

				try {
					br.close();

				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;

				}
			}
		}
		return true;
	}

	public static boolean fileWrite(String directry, String OutfileName, HashMap<String,String> branchMap,HashMap<String,Long> salesMap) {
		BufferedWriter bw = null;

		try {
			File file1 = new File (directry,OutfileName);
			FileWriter fw = new FileWriter (file1);
			bw = new BufferedWriter(fw);

			for(String branchCoad : branchMap.keySet()) {
				bw.write(branchCoad + "," + branchMap.get(branchCoad) + "," + salesMap.get(branchCoad));
				bw.newLine();
			}

		}catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;

		}finally {
			if(bw != null) {

				try {
					bw.close();

				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;

				}
			}
		}
		return true;
	}
}
